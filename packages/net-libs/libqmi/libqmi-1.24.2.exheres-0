# Copyright 2013 Paul Seidler
# Distributed under the terms of the GNU General Public License v2

require udev-rules

SUMMARY="Library for talking to devices with Qualcomm MSM Interface (QMI) protocol"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"
DOWNLOADS="https://www.freedesktop.org/software/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gtk-doc
    mbim [[ description = [ QMI over MBIM, required by recent qualcomm modems ] ]]
"

# Wants to access /dev/virtual/qmi*
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-apps/help2man
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/glib:2[>=2.36]
        gnome-desktop/libgudev[>=147]
        mbim? ( net-libs/libmbim[>=1.18.0] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-firmware-update
    --enable-mm-runtime-check
    --disable-static
    --with-udev
    --with-udev-base-dir="${UDEVDIR}"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gtk-doc' 'mbim mbim-qmux' )

