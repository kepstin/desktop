# Copyright 2016-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PV##*_p}

require launchpad autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ] \
    gsettings \
    vala [ vala_dep=true ]

SUMMARY="Library for instrumenting and integrating with all aspects of the Unity shell"
DOWNLOADS="https://bazaar.launchpad.net/~unity-team/${PN}/trunk/tarball/${MY_PNV} -> ${PNV}.tar.gz"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="gobject-introspection"

# Tests fail to build
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        virtual/pkg-config[>=0.16]
    build+run:
        dev-libs/glib:2[>=2.32]
        dev-libs/dee[>=1.2.5]
        dev-libs/libdbusmenu:0.4[>=0.3.93][vapi]
        x11-libs/gtk+:3[>=3.4.1]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.10.0] )
"

WORK=${WORKBASE}/\~unity-team/${PN}/trunk

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Fix-FTB-with-recent-vala-requiring-non-public-abstra.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-c-warnings
    --disable-docs
    --disable-headless-tests
    --disable-integration-tests
    --disable-lttng
    --disable-static
    --disable-trace-log
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gobject-introspection introspection' )

src_prepare() {
    autotools_src_prepare

    edo intltoolize --force --automake
}

